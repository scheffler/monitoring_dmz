# Overview 
This ansible repository is for an installation of the Icinga2 core infrastructure @PSI using ansible playbooks. The repository contains the following ansible playbooks to install:
- Two Mariadb database server in a master/master replication with one cluster address and a keep allive service to switch the cluster address (IP) if one server fails.
- Two Icinga2 Master server, with load balancing and failover for all Icinga2 Master tasks. Also including Icingaweb2 framework with a default configuration and Icinga Director.
- Icinga2 Clients, just for core Icinga2 servers 
- Special plugins for the Icinga2 agents.
- Influxidb / grafana server for historical monitoring data
<br>
The monitoring repository:<br>
The particular installations of the whole Icinga2 infrastructure are devided in: Icinga2 core infrastructure, satellites for a security level and satellites in a special subnet inside a security level. The base is this monitoring repository with all it's branches, which will never merged. The branches reflecting all Icinga2 zones, because every Icinga2 zone must have it's satellites (at least 2, max. n), which needs their own special installations. Why the satellite installations differs? In addition to the zone-specific configuration, the satellites also have different tasks with different plugins and different security criteria. Because of this restrictions, the satellite configuration differs more and more and at one point one branch for all satellites would be impracticable.

The current branches are:
```
  remotes/origin/master

# DEV environment
  remotes/origin/zone-lv3-dev-satellites
  remotes/origin/zone-lv5-net-dev-satellites
  remotes/origin/zone-ait-dev-masters

# Production
  remotes/origin/zone-ait-masters
  remotes/origin/zone-lv2-satellites
  remotes/origin/zone-lvx-satellites
  remotes/origin/zone-lv4-satellites
  remotes/origin/zone-lv5-net-satellites
  remotes/origin/zone-lv5-satellites
  remotes/origin/zone-lv6-satellites
```
Ansible controller for a particular installation is:
- The first satellite, normaly vmonsat-lvx-01.psi.ch, of all satellites for a security level. The checked out branch must be _zone-lv(n)-satellites_
- monsat-lv3-01.psi.ch for the Icinga2 core infrastructure. The checked out branch must be _zone-ait-masters_

## Documentation
The documentation in README.md differs from branch to branch. This master branch, without any playbooks and roles, consits just of this README.md with an overview of the monitoring repo. All other branches contains a separate README.md for it's tasks.
